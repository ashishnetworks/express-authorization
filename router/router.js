var router = require("express").Router()
var passportOfficer = require("../helpers/passport.officer")


router.get("/login",function(req,res){
    res.render("login") //this parameter is the file name which exist in views dir. With ejs extension
})

//This is where login page should send its details
router.post("/login",passportOfficer.authenticate("local",{
    successRedirect: "/", //redirects to this page after successfull login
    failureRedirect: "/login" //redirects here after unsuccessfull login
}))


router.get("/",function(req,res,next) {

    req.session.a = "ashish" // This is to store some random session variable
    //Works even when user is not logged in

    console.log(req.user)
    console.log(req.session)

    if(req.user) { //checks if user is logged in or not?

        res.send("user SIGNED in")

        //This logs the user out
        req.session.destroy(function(){
            //To do soemthing after logging user out
            // May be we should also clear the cookies here
        })
    }else {
        //when user is not logged in
        res.send("user NOT signed in")
    }
})


module.exports = router