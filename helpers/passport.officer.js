var passport = require("passport")
var LocalStrategy = require("passport-local").Strategy

var authDict = {
    "ashish": {
        "id":"1234",
        "username": "ashish",
        "password": "abcd"
    }
}

passport.use(new LocalStrategy(
    function(username, password, done) {

        console.log("Executed checking function")
        //Usually some db should be used to store user credentials
        let user = authDict[username] || {}
        if(user.password == password) {
            console.log("Auth successfull")
            return done(null, user);
        }

        console.log("Auth unsuccessfull")
        return done(null, false, { message: 'username or password is incorrect' });
    }
));

passport.serializeUser(function(user, done) {

    console.log("came inside serialization")
    //the value of second parameter will be stored in passport.user in req.session
    done(null, user);
});

passport.deserializeUser(function(user, done) {

    console.log("came inside deserilization")
    //the 2nd value/object which will be assigned to req.user
    done(null, user.id);
});

module.exports = passport