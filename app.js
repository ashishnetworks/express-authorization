"use strict";

var express = require("express")
var app = express()
var index = require("./router/router")

var http = require("http")
var path = require("path")

var cookieParser = require('cookie-parser');
var passport = require('passport')
var expressSession = require("express-session")


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs'); //ejs module must be seprately be installed

//now path under static will be mounted to first parameter path
app.use('/static',express.static("./views/static"))

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(cookieParser()); // This parses the cookie from header to req.cookies

// Secrate key is used to create hmac hash when cookie values are set.
// This is to make sure if user modies something hmac wont match

//express.session middleware is used to retrieve user session from a datastore (like Redis).
// We can find the session object because the session Id is stored in the cookie,
// which is provided to the server with every request.
app.use(expressSession({ secret: 'mySecrateKey', resave: true, saveUninitialized: true}));

app.use(passport.initialize())

//Should be always after express session
//This is necessary for mkaing user authentication possible, but it also needs passport.sirialise
// and passport.desserialise methods to be initiated
app.use(passport.session())

app.use("/",index)

//When error is caught no where, it will be caugh here, so this should be last function
app.use(function(err,req,res,next){
    if(err) {
        res.send(`Caught in last error hander : ${err}`)
    }
})

let server = http.createServer(app)
server.listen(3000,function(){
    console.log("Started listening")
})